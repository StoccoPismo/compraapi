package com.entrevista.core.service;

import com.entrevista.CompraApplicationTests;
import com.entrevista.core.entity.Carrinho;
import com.entrevista.core.entity.User;
import com.entrevista.core.enums.CarrinhoStatus;
import com.entrevista.core.repository.CarrinhoRepository;
import com.entrevista.core.repository.UserRepository;
import io.jsonwebtoken.lang.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.UUID;

import static org.junit.Assert.*;

/**
 * Created by stocco on 24/06/17.
 */
public class UserServiceTest  extends CompraApplicationTests{

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private CarrinhoRepository carrinhoRepository;

    private User user;
    private String hashTest;

    @Before
    public void setUp() throws Exception {

        this.hashTest = UUID.randomUUID().toString() + "@test.com";
        this.user = new User()
                .setEmail(this.hashTest)
                .setName(this.hashTest)
                .setPassword("123")
                .encriptPassword();
    }

    @Test
    public void createUser() throws Exception {

        userRepository.save(this.user);
        User userSalvo = userRepository.findByEmail(this.hashTest);
        Assert.notNull(userSalvo);

        Carrinho carrinho = new Carrinho();
        carrinho.setCarrinhoStatus(CarrinhoStatus.ATIVO);
        carrinho.setUser(user);
        carrinho.setValorTotal(0.0);
        carrinhoRepository.save(carrinho);




    }

}