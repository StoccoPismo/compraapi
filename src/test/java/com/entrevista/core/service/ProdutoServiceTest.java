package com.entrevista.core.service;

import com.entrevista.CompraApplicationTests;
import com.entrevista.core.dto.ProdutoDTO;
import com.entrevista.core.entity.Produto;
import com.entrevista.core.repository.ProdutoRepository;
import com.entrevista.core.utils.RandomizingUtil;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by stocco on 24/06/17.
 */
public class ProdutoServiceTest extends CompraApplicationTests {

    @Autowired
    private ProdutoRepository produtoRepository;

    private ProdutoDTO produtoDTO;

    @Before
    public void setUp() throws Exception {
        this.produtoDTO = RandomizingUtil.getRandomizer().nextObject(ProdutoDTO.class).setId(null);
    }

    @Test
    public void saveProduto() throws Exception {

        Produto produto = produtoRepository.findByNome(produtoDTO.getNome());
        Assert.assertNull(produto);

        produto = new Produto();
        BeanUtils.copyProperties(produtoDTO, produto, "id");
        produtoRepository.save(produto);

        Produto produtoSalvo = produtoRepository.findByNome(produtoDTO.getNome());
        Assert.assertNotNull(produtoSalvo);

    }

}