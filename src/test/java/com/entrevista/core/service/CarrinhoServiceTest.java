package com.entrevista.core.service;

import com.entrevista.CompraApplicationTests;
import com.entrevista.core.dto.ProdutoDTO;
import com.entrevista.core.dto.ProdutoDoCarrinhoDTO;
import com.entrevista.core.entity.Carrinho;
import com.entrevista.core.entity.ProdutoDoCarrinho;
import com.entrevista.core.entity.User;
import com.entrevista.core.enums.CarrinhoStatus;
import com.entrevista.core.repository.CarrinhoRepository;
import com.entrevista.core.repository.ProdutoDoCarrinhoRepository;
import com.entrevista.core.repository.UserRepository;
import org.joda.time.LocalDateTime;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.UUID;

/**
 * Created by stocco on 24/06/17.
 */

public class CarrinhoServiceTest extends CompraApplicationTests{

    @Autowired
    private CarrinhoRepository carrinhoRepository;
    @Autowired
    private ProdutoDoCarrinhoRepository produtoDoCarrinhoRepository;
    @Autowired
    private UserRepository userRepository;

    private ProdutoDTO produtoDTO;

    private User user;
    private User userTestAtualiza;
    private User userTestRemove;
    private User userTestCheckOut;


    private String hashTest;



    @Before
    public void setUp() throws Exception {

        this.hashTest = UUID.randomUUID().toString() + "@test.com";
        this.user = new User()
                .setEmail(this.hashTest)
                .setName(this.hashTest)
                .setPassword("123")
                .encriptPassword();
        userRepository.save(this.user);

        Carrinho carrinho = new Carrinho();
        carrinho.setCarrinhoStatus(CarrinhoStatus.ATIVO);
        carrinho.setUser(user);
        carrinho.setValorTotal(0.0);
        carrinhoRepository.save(carrinho);


        String hashTestDois = UUID.randomUUID().toString() + "@test2.com";
        this.userTestAtualiza = new User()
                .setEmail(hashTestDois)
                .setName(hashTestDois)
                .setPassword("123")
                .encriptPassword();
        userRepository.save(this.userTestAtualiza);

        Carrinho carrinhoTestAtualiza = new Carrinho();
        carrinhoTestAtualiza.setCarrinhoStatus(CarrinhoStatus.ATIVO);
        carrinhoTestAtualiza.setUser(this.userTestAtualiza);
        carrinhoTestAtualiza.setValorTotal(0.0);
        carrinhoRepository.save(carrinhoTestAtualiza);


        String hashTestTres = UUID.randomUUID().toString() + "@test3.com";
        this.userTestRemove = new User()
                .setEmail(hashTestTres)
                .setName(hashTestTres)
                .setPassword("123")
                .encriptPassword();
        userRepository.save(this.userTestRemove);

        Carrinho carrinhoTestRemove = new Carrinho();
        carrinhoTestRemove.setCarrinhoStatus(CarrinhoStatus.ATIVO);
        carrinhoTestRemove.setUser(this.userTestRemove);
        carrinhoTestRemove.setValorTotal(0.0);
        carrinhoRepository.save(carrinhoTestRemove);

        String hashTestQuatro = UUID.randomUUID().toString() + "@test4.com";
        this.userTestCheckOut = new User()
                .setEmail(hashTestQuatro)
                .setName(hashTestQuatro)
                .setPassword("123")
                .encriptPassword();
        userRepository.save(this.userTestCheckOut);

        Carrinho carrinhoTestCheckout = new Carrinho();
        carrinhoTestCheckout.setCarrinhoStatus(CarrinhoStatus.ATIVO);
        carrinhoTestCheckout.setUser(this.userTestCheckOut);
        carrinhoTestCheckout.setValorTotal(0.0);
        carrinhoRepository.save(carrinhoTestCheckout);


        Random r = new Random();
        int Low = 1;
        int High = 1000;
        int resultado = r.nextInt(High-Low) + Low;
        this.produtoDTO = new ProdutoDTO()
                .setId(1L)
                .setQuantidade(new Long(resultado));



    }


    @Test
    public void insereProdutoNoCarrinho() throws Exception {
        Carrinho carrinhoAtivo = carrinhoRepository.findByUserAndCarrinhoStatus(this.user, CarrinhoStatus.ATIVO);

        ProdutoDoCarrinho produtoDoCarrinho = new ProdutoDoCarrinho()
                .setCarrinho(carrinhoAtivo)
                .setProdutoId(1l)
                .setQuantidade(produtoDTO.getQuantidade())
                .setNomeProduto("produtoInicial")
                .setPreco(100D);
        produtoDoCarrinhoRepository.save(produtoDoCarrinho);
        carrinhoAtivo.calculaNovoValorCarrinho();
        Assert.assertEquals(this.produtoDTO.getQuantidade().longValue() * 100D, carrinhoAtivo.getValorTotal().doubleValue(), 0);

    }

    @Test
    public void atualizaProdutoDoCarrinho() throws Exception {
        Carrinho carrinhoAtivo = carrinhoRepository.findByUserAndCarrinhoStatus(userTestAtualiza, CarrinhoStatus.ATIVO);

        ProdutoDoCarrinho produtoDoCarrinho = new ProdutoDoCarrinho()
                .setCarrinho(carrinhoAtivo)
                .setProdutoId(1l)
                .setQuantidade(produtoDTO.getQuantidade())
                .setNomeProduto("produtoInicial")
                .setPreco(100D);
        produtoDoCarrinhoRepository.save(produtoDoCarrinho);
        carrinhoAtivo.calculaNovoValorCarrinho();
        Double valorAntigo = carrinhoAtivo.getValorTotal();

        produtoDoCarrinho.setQuantidade(produtoDoCarrinho.getQuantidade().longValue() + 2L);
        produtoDoCarrinhoRepository.save(produtoDoCarrinho);

        Carrinho carrinhoAtualizado = carrinhoRepository.findByUserAndCarrinhoStatus(this.userTestAtualiza, CarrinhoStatus.ATIVO);
        carrinhoAtualizado.calculaNovoValorCarrinho();

        Assert.assertEquals(valorAntigo.doubleValue() + (2L * 100D), carrinhoAtualizado.getValorTotal().doubleValue(), 0);

    }


    @Test
    public void removeProdutoDoCarrinho() throws Exception {

        Carrinho carrinhoAtivo = carrinhoRepository.findByUserAndCarrinhoStatus(userTestRemove, CarrinhoStatus.ATIVO);

        ProdutoDoCarrinho produtoDoCarrinho = new ProdutoDoCarrinho()
                .setCarrinho(carrinhoAtivo)
                .setProdutoId(1l)
                .setQuantidade(produtoDTO.getQuantidade())
                .setNomeProduto("produtoInicial2")
                .setPreco(300D);
        Long idDoProdutoDoCarrinho = produtoDoCarrinhoRepository.save(produtoDoCarrinho).getId();
        carrinhoAtivo.calculaNovoValorCarrinho();

        produtoDoCarrinhoRepository.delete(idDoProdutoDoCarrinho);
        carrinhoAtivo.getProdutosDoCarrinho().removeIf(carrinho -> produtoDoCarrinho.getId().intValue() == idDoProdutoDoCarrinho);
        carrinhoAtivo.calculaNovoValorCarrinho();
        Assert.assertTrue(carrinhoAtivo.getValorTotal().doubleValue() == 0);

    }

    @Test
    public void realizaCheckout() throws Exception {

        Carrinho carrinhoAtivo = carrinhoRepository.findByUserAndCarrinhoStatusFetchProdutosDoCarrinho(userTestCheckOut, CarrinhoStatus.ATIVO);
        ProdutoDoCarrinho produtoDoCarrinho = new ProdutoDoCarrinho()
                .setCarrinho(carrinhoAtivo)
                .setProdutoId(1l)
                .setQuantidade(produtoDTO.getQuantidade())
                .setNomeProduto("produtoInicial2")
                .setPreco(300D);
        produtoDoCarrinhoRepository.save(produtoDoCarrinho);
        carrinhoAtivo.calculaNovoValorCarrinho();
        carrinhoAtivo = carrinhoRepository.save(carrinhoAtivo);

        List<ProdutoDoCarrinhoDTO> produtosDTO = new ArrayList<>();
        for (ProdutoDoCarrinho produto: carrinhoAtivo.getProdutosDoCarrinho())
            produtosDTO.add(new ProdutoDoCarrinhoDTO(produto));


        carrinhoAtivo.setCarrinhoStatus(CarrinhoStatus.PAGO);
        Long carrinhoAntigoId = carrinhoRepository.save(carrinhoAtivo).getId();

        Carrinho novoCarrinho = new Carrinho()
                .setCarrinhoStatus(CarrinhoStatus.ATIVO)
                .setUser(userTestCheckOut)
                .setDataCriacao(LocalDateTime.now())
                .setValorTotal(0.0);
        carrinhoRepository.save(novoCarrinho);

        Assert.assertEquals(CarrinhoStatus.PAGO, carrinhoRepository.findOne(carrinhoAntigoId).getCarrinhoStatus());

        Assert.assertTrue(carrinhoRepository.findByUserAndCarrinhoStatusFetchProdutosDoCarrinho(userTestCheckOut, CarrinhoStatus.ATIVO).getId().longValue() != carrinhoAntigoId.longValue());


    }



}