package com.entrevista.core.utils;

import io.github.benas.randombeans.EnhancedRandomBuilder;
import io.github.benas.randombeans.api.EnhancedRandom;

import java.nio.charset.Charset;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.UUID;

public class RandomizingUtil {

    private static RandomizingUtil uniqueInstance = null;

    public static EnhancedRandom getRandomizer(){

        return  EnhancedRandomBuilder.aNewEnhancedRandomBuilder()
                .seed(Timestamp.valueOf(LocalDateTime.now()).getTime())
                .objectPoolSize(100)
                .randomizationDepth(3)
                .charset(Charset.forName("UTF-8"))
                .timeRange(LocalTime.MIN, LocalTime.MAX)
                .dateRange(LocalDate.now(), LocalDate.now().plusDays(1))
                .stringLengthRange(5, 10)
                .collectionSizeRange(1, 2)
                .scanClasspathForConcreteTypes(true)
                .overrideDefaultInitialization(false)
                .build();
    }

    public String getRandom16DigitsAsString(){
        return UUID.randomUUID().toString();
    }

    public Integer getRandom16DigitsAsInteger(){
        return Integer.parseInt(UUID.randomUUID().toString());
    }

}
