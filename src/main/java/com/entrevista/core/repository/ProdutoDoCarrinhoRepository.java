package com.entrevista.core.repository;

import com.entrevista.core.entity.ProdutoDoCarrinho;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by stocco on 24/06/17.
 */
public interface ProdutoDoCarrinhoRepository extends JpaRepository<ProdutoDoCarrinho, Long> {
}
