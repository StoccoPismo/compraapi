package com.entrevista.core.repository;

import com.entrevista.core.entity.Carrinho;
import com.entrevista.core.entity.User;
import com.entrevista.core.enums.CarrinhoStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * Created by stocco on 24/06/17.
 */
public interface CarrinhoRepository extends JpaRepository<Carrinho, Long> {


    Carrinho findByUserAndCarrinhoStatus(User user, CarrinhoStatus carrinhoStatus);

    @Query(value = "select c from Carrinho as c left join fetch c.produtosDoCarrinho as pdc " +
                    "where c.user = :user and c.carrinhoStatus = :carrinhoStatus")
    Carrinho findByUserAndCarrinhoStatusFetchProdutosDoCarrinho(@Param("user") User user, @Param("carrinhoStatus") CarrinhoStatus carrinhoStatus);

}
