package com.entrevista.core.repository;

import com.entrevista.core.entity.Produto;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by stocco on 24/06/17.
 */
public interface ProdutoRepository extends JpaRepository<Produto, Long> {

    Produto findByNome(String nome);

}
