package com.entrevista.core.dto;

import com.entrevista.core.entity.ProdutoDoCarrinho;

import javax.validation.constraints.NotNull;

/**
 * Created by stocco on 24/06/17.
 */
public class ProdutoDoCarrinhoDTO {

    @NotNull
    private Long produtoDoCarrinhoID;
    @NotNull
    private Long quantidadeDeProdutosNoCarrinho;

    private Double preco;


    public ProdutoDoCarrinhoDTO() {
    }


    public Double getPreco() {
        return preco;
    }

    public ProdutoDoCarrinhoDTO setPreco(Double preco) {
        this.preco = preco;
        return this;
    }


    public ProdutoDoCarrinhoDTO(ProdutoDoCarrinho produtoDoCarrinho) {
        this.produtoDoCarrinhoID = produtoDoCarrinho.getProdutoId();
        this.quantidadeDeProdutosNoCarrinho = produtoDoCarrinho.getQuantidade();
    }

    public Long getProdutoDoCarrinhoID() {
        return produtoDoCarrinhoID;
    }

    public ProdutoDoCarrinhoDTO setProdutoDoCarrinhoID(Long produtoDoCarrinhoID) {
        this.produtoDoCarrinhoID = produtoDoCarrinhoID;
        return this;
    }

    public Long getQuantidadeDeProdutosNoCarrinho() {
        return quantidadeDeProdutosNoCarrinho;
    }

    public ProdutoDoCarrinhoDTO setQuantidadeDeProdutosNoCarrinho(Long quantidadeDeProdutosNoCarrinho) {
        this.quantidadeDeProdutosNoCarrinho = quantidadeDeProdutosNoCarrinho;
        return this;
    }
}
