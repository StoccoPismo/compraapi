package com.entrevista.core.dto;

import com.entrevista.core.entity.ProdutoDoCarrinho;
import com.entrevista.core.entity.User;
import com.entrevista.core.enums.CarrinhoStatus;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.joda.time.LocalDateTime;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by stocco on 25/06/17.
 */
public class CarrinhoDTO {


    private Long id;

    private CarrinhoStatus carrinhoStatus;

    private List<ProdutoDoCarrinhoDTO> produtosDoCarrinho;

    private Double valorTotal;

    public CarrinhoDTO() {
        produtosDoCarrinho = new ArrayList<>();
    }

    public Long getId() {
        return id;
    }

    public CarrinhoDTO setId(Long id) {
        this.id = id;
        return this;
    }

    public CarrinhoStatus getCarrinhoStatus() {
        return carrinhoStatus;
    }

    public CarrinhoDTO setCarrinhoStatus(CarrinhoStatus carrinhoStatus) {
        this.carrinhoStatus = carrinhoStatus;
        return this;
    }

    public List<ProdutoDoCarrinhoDTO> getProdutosDoCarrinho() {
            return produtosDoCarrinho;
    }

    public CarrinhoDTO setProdutosDoCarrinho(List<ProdutoDoCarrinhoDTO> produtosDoCarrinho) {
        this.produtosDoCarrinho = produtosDoCarrinho;
        return this;
    }

    public Double getValorTotal() {
        return valorTotal;
    }

    public CarrinhoDTO setValorTotal(Double valorTotal) {
        this.valorTotal = valorTotal;
        return this;
    }

}
