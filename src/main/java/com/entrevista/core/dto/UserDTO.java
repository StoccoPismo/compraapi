package com.entrevista.core.dto;

/**
 * Created by stocco on 24/06/17.
 */
public class UserDTO {

    private String email;
    private String password;

    public String getEmail() {
        return email;
    }

    public UserDTO setEmail(String email) {
        this.email = email;
        return this;
    }

    public String getPassword() {
        return password;
    }

    public UserDTO setPassword(String password) {
        this.password = password;
        return this;
    }
}
