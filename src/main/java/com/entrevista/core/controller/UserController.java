package com.entrevista.core.controller;

import com.entrevista.configuration.security.AuthenticationResponse;
import com.entrevista.configuration.security.TokenUtils;
import com.entrevista.configuration.security.UserDetails;
import com.entrevista.core.dto.UserDTO;
import com.entrevista.core.service.UserService;
import com.entrevista.core.utils.TextUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mobile.device.Device;
import org.springframework.web.bind.annotation.*;

/**
 * Created by stocco on 24/06/17.
 */
@RestController
@RequestMapping("/users")
public class UserController {

    private final Logger logger = Logger.getLogger(this.getClass());

    @Autowired
    private UserService userService;
    @Autowired
    private TokenUtils tokenUtils;

    @RequestMapping(method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<?> createAccount(@RequestBody UserDTO userDTO, Device device){
        try{

            UserDetails userDetails = userService.createUser(userDTO);
            String token = tokenUtils.generateToken(userDetails, device);
            return new ResponseEntity(new AuthenticationResponse(token), HttpStatus.OK);
        }
        catch (Exception e) {
            logger.error(e.getMessage(), e);
            return new ResponseEntity(TextUtils.apiAnswerReturnMessage(e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
