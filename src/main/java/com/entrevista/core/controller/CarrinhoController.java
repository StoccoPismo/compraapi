package com.entrevista.core.controller;

import com.entrevista.core.dto.ProdutoDTO;
import com.entrevista.core.dto.ProdutoDoCarrinhoDTO;
import com.entrevista.core.service.CarrinhoService;
import com.entrevista.core.utils.TextUtils;
import org.apache.log4j.Logger;
import org.assertj.core.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * Created by stocco on 24/06/17.
 */
@RestController
@RequestMapping("/carrinhos")
public class CarrinhoController {

    private final Logger logger = Logger.getLogger(this.getClass());
    @Autowired
    private CarrinhoService carrinhoService;


    @RequestMapping(method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<?> adicionarProduto(){
        try{
            return new ResponseEntity(TextUtils.objectToJsonFormat(carrinhoService.buscaMeuCarrinho()), HttpStatus.OK);
        }
        catch (Exception e) {
            logger.error(e.getMessage(), e);
            return new ResponseEntity(TextUtils.apiAnswerReturnMessage(e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }




    @RequestMapping(method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<?> adicionarProduto(@RequestBody ProdutoDTO produtoDTO){
        try{

            if(Strings.isNullOrEmpty(produtoDTO.getNome()) || produtoDTO.getQuantidade() == null)
                return new ResponseEntity(TextUtils.apiAnswerReturnMessage("Conteudo invalido do json!"), HttpStatus.BAD_REQUEST);

            Long produtoDoCarrinhoId = carrinhoService.insereProdutoNoCarrinho(produtoDTO.getNome(), produtoDTO.getQuantidade());
            return new ResponseEntity(TextUtils.apiAnswerReturnMessage("Produto inserido no Carrinho com id:" + produtoDoCarrinhoId), HttpStatus.OK);
        }
        catch (Exception e) {
            logger.error(e.getMessage(), e);
            return new ResponseEntity(TextUtils.apiAnswerReturnMessage(e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @RequestMapping(value = "/checkout", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<?> checkOut(){
        try{
            carrinhoService.realizaCheckout();
            return new ResponseEntity(TextUtils.apiAnswerReturnMessage("Compra realizada com sucesso!"), HttpStatus.OK);
        }
        catch (Exception e) {
            logger.error(e.getMessage(), e);
            return new ResponseEntity(TextUtils.apiAnswerReturnMessage(e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @RequestMapping(value = "{produtoDoCarrinhoId}", method = RequestMethod.DELETE)
    @ResponseBody
    public ResponseEntity<?> removerProdutoDoCarrinho(@PathVariable("produtoDoCarrinhoId") Long produtoDoCarrinhoId){
        try{

            if(produtoDoCarrinhoId < 1)
                return new ResponseEntity(TextUtils.apiAnswerReturnMessage("Conteudo invalido do json!"), HttpStatus.BAD_REQUEST);

            carrinhoService.removeProdutoDoCarrinho(produtoDoCarrinhoId);
            return new ResponseEntity(TextUtils.apiAnswerReturnMessage("Produto removido do Carrinho!"), HttpStatus.OK);
        }
        catch (Exception e) {
            logger.error(e.getMessage(), e);
            return new ResponseEntity(TextUtils.apiAnswerReturnMessage(e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(method = RequestMethod.PUT)
    @ResponseBody
    public ResponseEntity<?> atualizaQuantidadeDoProdutoNoCarrinho(@Validated @RequestBody ProdutoDoCarrinhoDTO produtoDoCarrinhoDTO){
        try{

            carrinhoService.atualizaProdutoDoCarrinho(produtoDoCarrinhoDTO);
            return new ResponseEntity(TextUtils.apiAnswerReturnMessage("Quantidade do Produto atualizada!"), HttpStatus.OK);
        }
        catch (Exception e) {
            logger.error(e.getMessage(), e);
            return new ResponseEntity(TextUtils.apiAnswerReturnMessage(e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


}
