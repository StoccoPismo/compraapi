package com.entrevista.core.entity;

import com.entrevista.core.enums.CarrinhoStatus;
import org.joda.time.LocalDateTime;
import org.springframework.cache.annotation.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by stocco on 24/06/17.
 */
@Entity
@org.springframework.cache.annotation.Cacheable
public class Carrinho {
    @GeneratedValue( strategy = GenerationType.AUTO)
    @Id
    private Long id;

    @JoinColumn(name = "status")
    @Enumerated(EnumType.STRING)
    private CarrinhoStatus carrinhoStatus;

    @ManyToOne
    private User user;

    @OneToMany(mappedBy = "carrinho", fetch = FetchType.LAZY)
    private List<ProdutoDoCarrinho> produtosDoCarrinho;

    private Double valorTotal;

    private LocalDateTime dataCriacao;

    public void calculaNovoValorCarrinho(){

        double valorTotal = 0;
        for (ProdutoDoCarrinho produtoDoCarrinho: getProdutosDoCarrinho())
            valorTotal = valorTotal + produtoDoCarrinho.getValorTotal().doubleValue();

        setValorTotal(new Double(valorTotal));
    }


    public LocalDateTime getDataCriacao() {
        return dataCriacao;
    }

    public Carrinho setDataCriacao(LocalDateTime dataCriacao) {
        this.dataCriacao = dataCriacao;
        return this;
    }

    public List<ProdutoDoCarrinho> getProdutosDoCarrinho() {

        if(produtosDoCarrinho != null)
            return produtosDoCarrinho;

        return new ArrayList<>();
    }

    public Carrinho setProdutosDoCarrinho(List<ProdutoDoCarrinho> produtosDoCarrinho) {
        this.produtosDoCarrinho = produtosDoCarrinho;
        return this;
    }

    public Double getValorTotal() {
        return valorTotal;
    }

    public User getUser() {
        return user;
    }

    public Carrinho setUser(User user) {
        this.user = user;
        return this;
    }



    public Carrinho setValorTotal(Double valorTotal) {
        this.valorTotal = valorTotal;
        return  this;
    }

    public Long getId() {
        return id;
    }

    public Carrinho setId(Long id) {
        this.id = id;
        return this;
    }

    public CarrinhoStatus getCarrinhoStatus() {
        return carrinhoStatus;
    }

    public Carrinho setCarrinhoStatus(CarrinhoStatus carrinhoStatus) {
        this.carrinhoStatus = carrinhoStatus;
        return this;
    }
}
