package com.entrevista.core.entity;

import javax.persistence.*;

/**
 * Created by stocco on 24/06/17.
 */
@Entity
public class ProdutoDoCarrinho {

    @Id
    @GeneratedValue
    private Long id;
    private Long quantidade;
    private Long produtoId;

    @ManyToOne
    @JoinColumn(name="carrinho_id")
    private Carrinho carrinho;


    private Double preco;

    private String nomeProduto;


    public Long getId() {
        return id;
    }

    public ProdutoDoCarrinho setId(Long id) {
        this.id = id;
        return this;
    }

    public Long getProdutoId() {
        return produtoId;
    }

    public ProdutoDoCarrinho setProdutoId(Long produtoId) {
        this.produtoId = produtoId;
        return this;
    }

    public Double getValorTotal(){
        return new Double(getPreco().doubleValue() * getQuantidade().intValue());
    }

    public Long getQuantidade() {
        return quantidade;
    }

    public ProdutoDoCarrinho setQuantidade(Long quantidade) {
        this.quantidade = quantidade;
        return this;
    }

    public Carrinho getCarrinho() {
        return carrinho;
    }

    public ProdutoDoCarrinho setCarrinho(Carrinho carrinho) {
        this.carrinho = carrinho;
        return this;
    }

    public Double getPreco() {
        return preco;
    }

    public ProdutoDoCarrinho setPreco(Double preco) {
        this.preco = preco;
        return this;
    }

    public String getNomeProduto() {
        return nomeProduto;
    }

    public ProdutoDoCarrinho setNomeProduto(String nomeProduto) {
        this.nomeProduto = nomeProduto;
        return this;
    }
}
