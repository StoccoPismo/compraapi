package com.entrevista.core.entity;

public interface AbstractUser {
    String getEmail();
}
