package com.entrevista.core.entity;

import org.springframework.cache.annotation.*;

import javax.persistence.*;
import java.util.List;

@Entity
@org.springframework.cache.annotation.Cacheable
public class Produto {

    @GeneratedValue( strategy = GenerationType.AUTO)
    @Id
    private Long id;
    private Long quantidade;

    private String nome;

    private Double preco;

    private boolean ativo;


    public boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }

    public String getNome() {
        return nome;
    }

    public Produto setNome(String nome) {
        this.nome = nome;
        return this;
    }

    public Long getId() {
        return id;
    }

    public Produto setId(Long id) {
        this.id = id;
        return this;
    }

    public Long getQuantidade() {
        return quantidade;
    }

    public Produto setQuantidade(Long quantidade) {
        this.quantidade = quantidade;
        return this;
    }

    public Double getPreco() {
        return preco;
    }

    public Produto setPreco(Double preco) {
        this.preco = preco;
        return this;
    }
}
