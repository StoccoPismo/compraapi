package com.entrevista.core.service;

import com.entrevista.core.dto.ProdutoDTO;
import com.entrevista.core.dto.ProdutoDoCarrinhoDTO;
import com.entrevista.core.entity.Produto;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by stocco on 24/06/17.
 */
@Service
public class ApiService {

    @Autowired
    private Environment env;

    private HttpClient httpClient = HttpClientBuilder.create().build();
    private ObjectMapper mapper = new ObjectMapper();

    public HttpResponse getProdutoInfoByNome(String produtoNome) throws Exception{

        String url = env.getRequiredProperty("produtoApi.url") + "produtos/nome/" + produtoNome;
        HttpGet get = new HttpGet(url);
        return httpClient.execute(get);

    }

    public HttpResponse realizaCheckoutNaApi(List<ProdutoDoCarrinhoDTO> produtosDoCarrinho) throws Exception{

        String url = env.getRequiredProperty("produtoApi.url") + "produtos/atualizaEstoquePosCompra";
        HttpPost post  = new HttpPost(url);
        StringEntity postingString = new StringEntity(mapper.writeValueAsString(produtosDoCarrinho));
        post.setEntity(postingString);
        post.setHeader("Content-type", "application/json");
        return httpClient.execute(post);

    }

}
