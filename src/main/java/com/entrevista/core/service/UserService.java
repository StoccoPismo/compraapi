package com.entrevista.core.service;


import com.entrevista.configuration.security.AuthorityConstant;
import com.entrevista.configuration.security.UserDetails;
import com.entrevista.core.dto.UserDTO;
import com.entrevista.core.entity.Carrinho;
import com.entrevista.core.entity.User;
import com.entrevista.core.enums.CarrinhoStatus;
import com.entrevista.core.repository.CarrinhoRepository;
import com.entrevista.core.repository.UserRepository;
import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private CarrinhoRepository carrinhoRepository;

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public UserDetails createUser(UserDTO userDTO) throws Exception{


        User user = new User()
                .setEmail(userDTO.getEmail())
                .setName(userDTO.getEmail())
                .setPassword(userDTO.getPassword())
                .encriptPassword();
        userRepository.save(user);


        Carrinho carrinho = new Carrinho()
            .setCarrinhoStatus(CarrinhoStatus.ATIVO)
            .setUser(user)
            .setDataCriacao(LocalDateTime.now())
            .setValorTotal(0.0);
        carrinhoRepository.save(carrinho);


        List<GrantedAuthority> authorityList = AuthorityUtils.createAuthorityList(AuthorityConstant.ROLE_USUARIO);
        UserDetails userDetails = new UserDetails(user, true, true, true, true, authorityList);

        return userDetails;
    }





}
