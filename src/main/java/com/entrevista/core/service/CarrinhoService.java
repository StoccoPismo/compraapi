package com.entrevista.core.service;

import com.entrevista.configuration.security.UserContext;
import com.entrevista.core.dto.CarrinhoDTO;
import com.entrevista.core.dto.ProdutoDTO;
import com.entrevista.core.dto.ProdutoDoCarrinhoDTO;
import com.entrevista.core.entity.Carrinho;
import com.entrevista.core.entity.ProdutoDoCarrinho;
import com.entrevista.core.enums.CarrinhoStatus;
import com.entrevista.core.repository.CarrinhoRepository;
import com.entrevista.core.repository.ProdutoDoCarrinhoRepository;
import com.entrevista.core.utils.TextUtils;
import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;
import org.joda.time.LocalDateTime;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by stocco on 24/06/17.
 */
@Service
public class CarrinhoService {

    private final Logger logger = Logger.getLogger(this.getClass());

    @Autowired
    private CarrinhoRepository carrinhoRepository;
    @Autowired
    private ProdutoDoCarrinhoRepository produtoDoCarrinhoRepository;

    @Autowired
    private ApiService apiService;


    @Transactional()
    public Long insereProdutoNoCarrinho(String nomeDoProduto, Long quantidade) throws Exception{

        Carrinho carrinhoAtivo = carrinhoRepository.findByUserAndCarrinhoStatusFetchProdutosDoCarrinho(UserContext.getLoggedUser(), CarrinhoStatus.ATIVO);


        HttpResponse response = apiService.getProdutoInfoByNome(nomeDoProduto);
        validaRespostaApi(response);

        ProdutoDTO produtoDaApiDTO = (ProdutoDTO)TextUtils.parseJsonToObject(EntityUtils.toString(response.getEntity()), ProdutoDTO.class);

        ProdutoDoCarrinho produtoDoCarrinho = new ProdutoDoCarrinho()
                .setCarrinho(carrinhoAtivo)
                .setQuantidade(quantidade)
                .setNomeProduto(nomeDoProduto)
                .setPreco(produtoDaApiDTO.getPreco())
                .setProdutoId(produtoDaApiDTO.getId());
        produtoDoCarrinho = produtoDoCarrinhoRepository.save(produtoDoCarrinho);

        carrinhoAtivo.getProdutosDoCarrinho().add(produtoDoCarrinho);
        recalculaValorCarrinho(carrinhoAtivo);
        return  produtoDoCarrinho.getId();

    }


    @Transactional()
    public void realizaCheckout() throws Exception{

        Carrinho carrinhoAtivo = carrinhoRepository.findByUserAndCarrinhoStatusFetchProdutosDoCarrinho(UserContext.getLoggedUser(), CarrinhoStatus.ATIVO);
        List<ProdutoDoCarrinhoDTO> produtosDTO = new ArrayList<>();
        for (ProdutoDoCarrinho produto: carrinhoAtivo.getProdutosDoCarrinho())
            produtosDTO.add(new ProdutoDoCarrinhoDTO(produto));

        if(produtosDTO.isEmpty())
            throw new Exception("Carrinho sem items para concluir o checkout!");

        HttpResponse response = apiService.realizaCheckoutNaApi(produtosDTO);
        validaRespostaApi(response);
        EntityUtils.consumeQuietly(response.getEntity());

        carrinhoAtivo.setCarrinhoStatus(CarrinhoStatus.PAGO);
        carrinhoRepository.save(carrinhoAtivo);

        Carrinho novoCarrinho = new Carrinho()
                .setCarrinhoStatus(CarrinhoStatus.ATIVO)
                .setUser(UserContext.getLoggedUser())
                .setDataCriacao(LocalDateTime.now())
                .setValorTotal(0.0);
        carrinhoRepository.save(novoCarrinho);

    }

    private void validaRespostaApi(HttpResponse response) throws Exception{
        if(response.getStatusLine().getStatusCode() == 204){
            EntityUtils.consumeQuietly(response.getEntity());
            throw new Exception("Produto não encontrado!");
        }
        if(response.getStatusLine().getStatusCode() != 200) {
            EntityUtils.consumeQuietly(response.getEntity());
            throw new Exception("Problemas na API ou na conexão!");
        }
    }


    @Transactional()
    public void removeProdutoDoCarrinho(Long produtoDoCarrinhoId) throws Exception{

        produtoDoCarrinhoRepository.delete(produtoDoCarrinhoId);
        Carrinho carrinhoAtivo = carrinhoRepository.findByUserAndCarrinhoStatusFetchProdutosDoCarrinho(UserContext.getLoggedUser(), CarrinhoStatus.ATIVO);

        carrinhoAtivo.getProdutosDoCarrinho().removeIf(produtoDoCarrinho -> produtoDoCarrinho.getId().intValue() == produtoDoCarrinhoId);
        recalculaValorCarrinho(carrinhoAtivo);
    }

    @Transactional()
    public void atualizaProdutoDoCarrinho(ProdutoDoCarrinhoDTO produtoDoCarrinhoDTO) throws Exception{

        ProdutoDoCarrinho produtoDoCarrinho = produtoDoCarrinhoRepository.findOne(produtoDoCarrinhoDTO.getProdutoDoCarrinhoID());
        produtoDoCarrinho.setQuantidade(produtoDoCarrinho.getQuantidade().longValue() + produtoDoCarrinhoDTO.getQuantidadeDeProdutosNoCarrinho().longValue());
        produtoDoCarrinhoRepository.save(produtoDoCarrinho);

        Carrinho carrinhoAtivo = carrinhoRepository.findByUserAndCarrinhoStatusFetchProdutosDoCarrinho(UserContext.getLoggedUser(), CarrinhoStatus.ATIVO);
        recalculaValorCarrinho(carrinhoAtivo);

    }
    private void recalculaValorCarrinho(Carrinho carrinho){
        carrinho.calculaNovoValorCarrinho();
        carrinhoRepository.save(carrinho);
    }


    @Transactional(readOnly = true)
    public CarrinhoDTO buscaMeuCarrinho(){

        Carrinho carrinhoAtivo = carrinhoRepository.findByUserAndCarrinhoStatusFetchProdutosDoCarrinho(UserContext.getLoggedUser(), CarrinhoStatus.ATIVO);
        CarrinhoDTO carrinhoDTO = new CarrinhoDTO();
        BeanUtils.copyProperties(carrinhoAtivo, carrinhoDTO, "produtosDoCarrinho");


        for (ProdutoDoCarrinho produtoDocarrinho: carrinhoAtivo.getProdutosDoCarrinho()) {
            ProdutoDoCarrinhoDTO produtoDoCarrinhoDTO = new ProdutoDoCarrinhoDTO()
                    .setProdutoDoCarrinhoID(produtoDocarrinho.getId())
                    .setPreco(produtoDocarrinho.getPreco())
                    .setQuantidadeDeProdutosNoCarrinho(produtoDocarrinho.getQuantidade());
            carrinhoDTO.getProdutosDoCarrinho().add(produtoDoCarrinhoDTO);
        }

        return carrinhoDTO;

    }




}
