package com.entrevista.core.enums;

/**
 * Created by stocco on 24/06/17.
 */
public enum CarrinhoStatus {
    ATIVO, PAGO
}
