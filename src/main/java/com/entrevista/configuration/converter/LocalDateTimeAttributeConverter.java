package com.entrevista.configuration.converter;

import org.joda.time.LocalDateTime;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.sql.Timestamp;

@Converter(autoApply = true)
public class LocalDateTimeAttributeConverter implements AttributeConverter<LocalDateTime, Timestamp> {

    @Override
    public Timestamp convertToDatabaseColumn(LocalDateTime value) {
        if(value==null) return null;
        return new Timestamp(value.toDate().getTime());
    }

    @Override
    public LocalDateTime convertToEntityAttribute(Timestamp value) {
        if(value==null) return null;
        return new LocalDateTime(value.getTime());
    }
}