package com.entrevista.configuration.security;


import com.entrevista.core.entity.AbstractUser;
import com.entrevista.core.entity.User;
import org.springframework.security.core.GrantedAuthority;

import java.util.List;


public class UserDetails extends org.springframework.security.core.userdetails.User implements AbstractUser {

	private Long id;

	private String name;
	private String email;


	@Override
	public String toString() {
		return "UserDetails{" +
				"id=" + id +
				", name='" + name + '\'' +
				", email='" + email + '\'' +
				'}';
	}

	@Override
	public String getEmail() {
		return this.getUsername();
	}

	public UserDetails(User user, boolean enabled, boolean accountNonExpired, boolean credentialsNonExpired, boolean accountNonLocked, List<GrantedAuthority> authorityList) {

		super(user.getEmail(), user.getPassword(), enabled, accountNonExpired, credentialsNonExpired, accountNonLocked, authorityList);
		this.id = user.getId();
		this.name = user.getName();
	}


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setEmail(String email) {
		this.email = email;
	}
}
